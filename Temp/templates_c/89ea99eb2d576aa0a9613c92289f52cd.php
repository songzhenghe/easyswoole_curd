<?php /*a:1:{s:48:"/home/szh/easyswoole/App/Views/Upload/index.html";i:1575963841;}*/ ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>swoole分段上传</title>
<script src="https://cdn.bootcss.com/jquery/3.4.1/jquery.min.js"></script>
</head>
<body>

<input type="file" name="" id="file">
<button id="upload">上传</button>
<span id="output" style="font-size:12px">等待</span>


<script>

$('#upload').click(function(){
	if(!$("#file")[0].files[0]){
		alert('请先选择文件');
		return;	
	}
	var file = $("#file")[0].files[0],  //文件对象
		name = file.name,        //文件名
		size = file.size,        //总大小
		succeed = 0;
	var shardSize = 2 * 1024 * 1024,    //以2MB为一个分片
		shardCount = Math.ceil(size / shardSize);  //总片数
		
		
	window.start=function(i){
		//计算每一片的起始与结束位置
		var start = i * shardSize,
			end = Math.min(size, start + shardSize);
		//构造一个表单，FormData是HTML5新增的
		var form = new FormData();
		form.append("data", file.slice(start,end));  //slice方法用于切出文件的一部分
		form.append("name", name);
		form.append("total", shardCount);  //总片数
		form.append("index", i + 1);        //当前是第几片
		form.append("fileSize", file.size); //文件大小
		//Ajax提交
		$.ajax({
			url: "<?php echo htmlentities($server); ?>",
			type: "POST",
			data: form,
			async: true,        //异步
			processData: false,  //很重要，告诉jquery不要对form进行处理
			contentType: false,  //很重要，指定为false才能形成正确的Content-Type
			dataType: 'json',
			success: function(data){
				if(data.result.status!='ok'){
					alert('上传失败');
					return;	
				}
				++succeed;
				$("#output").text(succeed + " / " + shardCount + " [" + Math.round(succeed/shardCount*100) + "%]");
				++i;
				if(i < shardCount) window.start(i);
			},
			error: function(){
				alert('上传失败');	
			}
		});
	}
	
	start(0);	
});

</script>
</body>
</html>
