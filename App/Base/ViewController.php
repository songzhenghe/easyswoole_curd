<?php
 
namespace App\Base;
 
use \EasySwoole\EasySwoole\Config;
use EasySwoole\Http\AbstractInterface\Controller;
use EasySwoole\Http\Request;
use EasySwoole\Http\Response;
use EasySwoole\Http\Session\Session;
use think\Template;
 
/**
 * 视图控制器
 * Class ViewController
 * @author  : evalor <master@evalor.cn>
 * @package App
 */
abstract class ViewController extends Controller
{
    private $view;
     
    /**
     * 初始化模板引擎
     * ViewController constructor.
     * @param string   $actionName
     * @param Request  $request
     * @param Response $response
     */
    public function __construct()
    {
        
//        var_dump($this->view);
        parent::__construct();
        $this->init();
    }
    /**
     * 获取用户的真实IP
     * @param string $headerName 代理服务器传递的标头名称
     * @return string
     */
    protected function clientRealIP($headerName = 'x-real-ip')
    {
        $server = ServerManager::getInstance()->getSwooleServer();
        $client = $server->getClientInfo($this->request()->getSwooleRequest()->fd);
        $clientAddress = $client['remote_ip'];
        $xri = $this->request()->getHeader($headerName);
        $xff = $this->request()->getHeader('x-forwarded-for');
        if ($clientAddress === '127.0.0.1') {
            if (!empty($xri)) {  // 如果有xri 则判定为前端有NGINX等代理
                $clientAddress = $xri[0];
            } elseif (!empty($xff)) {  // 如果不存在xri 则继续判断xff
                $list = explode(',', $xff[0]);
                if (isset($list[0])) $clientAddress = $list[0];
            }
        }
        return $clientAddress;
    }
    protected function input($name, $default = null) {
        $value = $this->request()->getRequestParam($name);
        return $value ?? $default;
    } 
    /**
     * 输出模板到页面
     * @param  string|null $template 模板文件
     * @param array        $vars 模板变量值
     * @param array        $config 额外的渲染配置
     * @author : evalor <master@evalor.cn>
     */
    public function fetch($template=null, $vars = [], $config = [])
    {
        ob_start();
        $template==null&&$template=$GLOBALS['base']['ACTION_NAME'];
        $this->view->fetch($template, $vars, $config);
        $content = ob_get_clean();
        $this->response()->write($content);
    }
     
    /**
     * @return Template
     */
    public function getView(): Template
    {
        return $this->view;
    }
     
    public function init()
    {
        $this->view             = new Template();
        $tempPath               = Config::getInstance()->getConf('TEMP_DIR');     # 临时文件目录
        $class_name_array       = explode('\\', static::class);
        $class_name_array_count = count($class_name_array);
        //$controller_path = $class_name_array[$class_name_array_count - 2] . DIRECTORY_SEPARATOR . $class_name_array[$class_name_array_count - 1] . DIRECTORY_SEPARATOR;
        $controller_path = $class_name_array[$class_name_array_count - 1] . DIRECTORY_SEPARATOR;
//        var_dump(static::class);
        $this->view->config([
                                'view_path' => EASYSWOOLE_ROOT . DIRECTORY_SEPARATOR . 'App' . DIRECTORY_SEPARATOR . 'Views' . DIRECTORY_SEPARATOR . $controller_path,
                                # 模板文件目录
                                'cache_path' => "{$tempPath}/templates_c/",               # 模板编译目录
                            ]);
         
//        var_dump(EASYSWOOLE_ROOT . DIRECTORY_SEPARATOR . 'App' . DIRECTORY_SEPARATOR . 'Views' . DIRECTORY_SEPARATOR . $controller_path);
    }
     
}