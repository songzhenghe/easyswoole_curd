<?php


namespace App\HttpController;


use EasySwoole\Http\AbstractInterface\Controller;
use EasySwoole\Http\Message\Status;
use EasySwoole\VerifyCode\Conf;

class Article extends \App\Base\ViewController
{
    public function index()
    {
        $this->response()->write('article/index');
    }

    protected function actionNotFound(?string $action)
    {
        $this->response()->withStatus(404);
        $file = EASYSWOOLE_ROOT.'/vendor/easyswoole/easyswoole/src/Resource/Http/404.html';
        if(!is_file($file)){
            $file = EASYSWOOLE_ROOT.'/src/Resource/Http/404.html';
        }
        $this->response()->write(file_get_contents($file));
    }
    public function lst(){
        $param = $this->request()->getRequestParam();
        $model = new \App\Model\ArticleModel();
        $where = [];
        $page = $param['page']??1;
        $pageSize = 2;
        $data = $model->order('itemid','desc')->limit(($page-1)*$pageSize,$pageSize)->withTotalCount()->select($where);
        $total = $model->lastQueryResult()->getTotalCount();
        $this->writeJson(Status::CODE_OK, $data, 'success');
    }
    public function detail(){
        $param = $this->request()->getRequestParam();
        $itemid = intval($param['itemid']);
        $model = new \App\Model\ArticleModel();
        $data = $model->where(['itemid'=>$itemid])->get();
        $this->writeJson(Status::CODE_OK, $data, 'success');
    }
    public function add(){
        $param = $this->request()->getRequestParam();
        $d=[
            'title'=>$param['title'],
            'content'=>$param['content'],
        ];
        $model = \App\Model\ArticleModel::create($d);
        $insertid = $model->save();
        $this->writeJson(Status::CODE_OK, ['insertid'=>$insertid], 'success');
    }
    public function mod(){
        $param = $this->request()->getRequestParam();
        $itemid = intval($param['itemid']);
        $d=[
            'title'=>$param['title'],
            'content'=>$param['content'],
        ];
        $model = \App\Model\ArticleModel::create()->get($itemid);
        $model->update($d);
        $affected_rows=$model->lastQueryResult()->getAffectedRows();
        $this->writeJson(Status::CODE_OK, ['affected_rows'=>$affected_rows], 'success');
    }
    public function del(){
        $param = $this->request()->getRequestParam();
        $itemid = intval($param['itemid']);
        $model = \App\Model\ArticleModel::create()->get($itemid);
        $model->destroy();
        $this->writeJson(Status::CODE_OK, [], 'success');
    }
    public function tpl_test(){
        $itemid = intval(1);
        $model = new \App\Model\ArticleModel();
        $data = $model->where(['itemid'=>$itemid])->get();
        $this->getView()->assign(['title'=>$data['title'],'content'=>$data['content']]);
        $this->fetch('index');
    }
    public function code(){
        $config = new Conf();
        $code = new \EasySwoole\VerifyCode\VerifyCode($config);
        $this->response()->withHeader('Content-Type','image/png');
        $this->response()->write($code->DrawCode()->getImageByte());
    }
    public function time(){
        /*
        // 每隔 1 秒执行一次
        \EasySwoole\Component\Timer::getInstance()->loop(1 * 1000, function () {
            echo "this timer runs at intervals of 1 seconds\n";
        });
        */
    }
}